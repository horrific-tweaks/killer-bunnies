This datapack re-introduces the [Killer Bunny](https://minecraft.fandom.com/wiki/Rabbit#The_Killer_Bunny) mob, which seeks revenge for hurting another bunny. If a player attacks a rabbit (provided there are no other players nearby), there is a small chance of spawning a Killer Bunny, which then attacks the player.

The bunny's targeting method often stays fixed on the first player it sees, so its behavior isn't quite as effective in multiplayer. However, as the bunny only spawns when there aren't other players nearby, this should not be a frequent issue.

![A screenshot of the Killer Bunny, with menacing red eyes.](./.images/bnuuy.png)
