# ----- find a good y-position to summon the bunny at
# if on a block and in air, then summon
execute unless block ~ ~-1 ~ minecraft:air if block ~ ~ ~ minecraft:air run function fennifith:bunny/rabbit/bunny_summon
# if in a block and not above air, then move up
execute unless block ~ ~-1 ~ minecraft:air unless block ~ ~ ~ minecraft:air positioned ~ ~1 ~ run function fennifith:bunny/rabbit/bunny_locate
# if above air, then move down
execute if block ~ ~-1 ~ minecraft:air positioned ~ ~-1 ~ run function fennifith:bunny/rabbit/bunny_locate
