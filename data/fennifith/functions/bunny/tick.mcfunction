# ----- Rabbit-related foods...
# Rabbit Stew
execute as @a[nbt={SelectedItem:{id:"minecraft:bowl"}}] if score @s bun.eaten matches 1.. run function fennifith:bunny/rabbit/rabbit_stew
scoreboard players set @a bun.eaten 0

execute as @a if score @s bun.hurt matches 1.. run function fennifith:bunny/rabbit/bunny_chance
scoreboard players set @a bun.hurt 0
